import {notFound, redirect} from "next/navigation";
import {getRequestConfig} from 'next-intl/server';
 
// Can be imported from a shared config
export const locales = ['en', 'cs'];
 
export default getRequestConfig(async ({locale}) => {
  if (locale === undefined) redirect('/en');
  // Validate that the incoming `locale` parameter is valid
  if (!locales.includes(locale)) notFound();
 
  return {
    messages: (await import(`./messages/${locale}.json`)).default
  };
});