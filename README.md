This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

See [this issue](https://github.com/amannn/next-intl/issues/250) with Turbopack and Intl and tell me, how to make it work :)